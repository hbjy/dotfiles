export ZSH="/home/hayden/.oh-my-zsh"

ZSH_THEME="zeit"

plugins=(
  git
  docker
  docker-compose
  node
  npm
  sudo
  systemd
  vscode
  web-search
  yarn
)

source $ZSH/oh-my-zsh.sh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export PATH="$PATH:$HOME/.yarn/bin:$HOME/.config/composer/vendor/bin"

export JAVA_HOME="/lib/jvm/java-11-openjdk"
alias config='/usr/bin/git --git-dir=/home/hayden/.cfg/ --work-tree=/home/hayden'

[[ -s "/home/hayden/.gvm/scripts/gvm" ]] && source "/home/hayden/.gvm/scripts/gvm"

alias vim="nvim"
alias vi="nvim"

GPG_TTY=`tty`
export GPG_TTY

export TERM=xterm
